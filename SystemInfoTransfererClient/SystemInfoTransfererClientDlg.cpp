/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// SystemInfoTransfererClientDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "SystemInfoTransfererClient.h"
#include "SystemInfoTransfererClientDlg.h"
#include "afxdialogex.h"
#include "ConnectSocket.h"
#include "VersionHelpers.h"
//#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//using namespace std;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSystemInfoTransfererClientDlg 대화 상자



CSystemInfoTransfererClientDlg::CSystemInfoTransfererClientDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SYSTEMINFOTRANSFERERCLIENT_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSystemInfoTransfererClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_List);
	DDX_Control(pDX, IDSEND, m_btnSend);
}

BEGIN_MESSAGE_MAP(CSystemInfoTransfererClientDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCONNECT, &CSystemInfoTransfererClientDlg::OnBnClickedConnect)
	ON_BN_CLICKED(IDSEND, &CSystemInfoTransfererClientDlg::OnBnClickedSend)
	ON_BN_CLICKED(IDDISCONNECT, &CSystemInfoTransfererClientDlg::OnBnClickedDisconnect)
	ON_WM_DESTROY()
	//ON_WM_CREATE()
	//ON_WM_CREATE()
	//ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDCLOSE, &CSystemInfoTransfererClientDlg::OnBnClickedClose)
END_MESSAGE_MAP()


// CSystemInfoTransfererClientDlg 메시지 처리기

BOOL CSystemInfoTransfererClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	pThread = nullptr;
	m_Socket.m_bIsConnected = false;
	m_Socket.m_bIsSending = false;
	m_fCPUUsage = 0;
	GetWindowRect(&m_DlgSize);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CSystemInfoTransfererClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CSystemInfoTransfererClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CSystemInfoTransfererClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSystemInfoTransfererClientDlg::OnBnClickedConnect()
{
	if (!m_Socket.m_bIsConnected) {
		m_Socket.Create();

		//if (m_Socket.Connect(_T(LOCALHOST), PORTNUM) == FALSE) {
		if (m_Socket.Connect(_T(SERVERIP), PORTNUM) == FALSE) {
			m_List.AddString(_T("서버에 접속할 수 없습니다."));
			m_List.SetCurSel(m_List.GetCount() - 1);

			AfxMessageBox(_T("ERROR: Failed to connect server"));
		}
		else {
			CString strMessage;

			m_Socket.m_bIsConnected = true;

			strMessage.Format(_T("서버에 접속했습니다."));
			m_List.AddString(strMessage);
			m_List.SetCurSel(m_List.GetCount() - 1);
		}
	}
	else {
		m_List.AddString(_T("이미 서버에 접속해 있습니다."));
		m_List.SetCurSel(m_List.GetCount() - 1);
	}
}

void CSystemInfoTransfererClientDlg::OnBnClickedDisconnect()
{
	if (m_Socket.m_bIsConnected) {
		m_Socket.m_bIsConnected = false;

		if (m_Socket.m_bIsSending) {
			m_Socket.m_bIsSending = false;
			m_btnSend.SetWindowTextW(_T("보내기"));
		}

		m_List.AddString(_T("서버와의 연결을 해제합니다."));
		m_List.SetCurSel(m_List.GetCount() - 1);

		m_Socket.ShutDown();
		m_Socket.Close();
	}
	else {
		m_List.AddString(_T("서버에 접속해 있지 않습니다."));
		m_List.SetCurSel(m_List.GetCount() - 1);
	}
}

void CSystemInfoTransfererClientDlg::OnBnClickedSend()
{
	if (m_Socket.m_bIsConnected) {
		if (!m_Socket.m_bIsSending) {
			if (AfxMessageBox(_T("서버에 PC 정보를 전송합니다."), MB_YESNO) == IDYES) {
				pThread = AfxBeginThread(SendPCInfo, this);
				m_Socket.m_bIsSending = true;
				m_btnSend.SetWindowTextW(_T("보내기 중지"));

				if (pThread == nullptr) {
					m_List.AddString(_T("서버로 전송할 수 없습니다."));
					m_List.SetCurSel(m_List.GetCount() - 1);

					m_Socket.m_bIsSending = false;
					m_btnSend.SetWindowTextW(_T("보내기"));
				}

				CloseHandle(pThread);
			}
		}
		else {
			m_Socket.m_bIsSending = false;
			m_btnSend.SetWindowTextW(_T("보내기"));
		}
	}
	else {
		m_List.AddString(_T("서버에 접속해 있지 않습니다."));
		m_List.SetCurSel(m_List.GetCount() - 1);
	}
}

void CSystemInfoTransfererClientDlg::OnBnClickedClose()
{
	PostQuitMessage(0);
}

UINT CSystemInfoTransfererClientDlg::SendPCInfo(LPVOID pParam) {
	CSystemInfoTransfererClientDlg *pDlg = (CSystemInfoTransfererClientDlg*)AfxGetApp()->m_pMainWnd;

	while (pDlg->m_Socket.m_bIsConnected && pDlg->m_Socket.m_bIsSending) {
		CString sendMessage;
		CString listMesage;

		pDlg->SetMemoryInfo();
		pDlg->SetProcessorInfo();
		pDlg->SetWindowsVersionInfo();

		pDlg->DrawCPUGraph();
		pDlg->DrawMemoryGraph();
		pDlg->Invalidate(FALSE);

		for (int i = 0; i < 60; i++) {
			if (i < 59) {
				pDlg->m_Socket.message[i] = pDlg->m_Socket.message[i + 1];
			}
			else { // i == 59
				pDlg->m_Socket.message[59].dMemTotal = pDlg->m_dMemTotal;
				pDlg->m_Socket.message[59].dAvailMemTotal = pDlg->m_dAvailMemTotal;
				pDlg->m_Socket.message[59].dVirtualTotal = pDlg->m_dVirtualTotal;
				pDlg->m_Socket.message[59].dMemInUse = pDlg->m_dMemTotal - pDlg->m_dAvailMemTotal;
				pDlg->m_Socket.message[59].fCPUUsage = pDlg->m_fCPUUsage;
				pDlg->m_Socket.message[59].nNumOfProcess = pDlg->m_nNumOfProcess;
				pDlg->m_Socket.message[59].strWindowsVersion = pDlg->m_strWindowsVersion;
			}
		}

		sendMessage.Format(_T("{\"windowsVersion\":\"%s\",\"numOfProcess\":\"%d\",\"cpuUsage\":\"%.1f\",\"memTotal\":\"%d\",\"availMemTotal\":\"%d\",\"virtualTotal\":\"%d\"}")
			, pDlg->m_Socket.message[59].strWindowsVersion, pDlg->m_Socket.message[59].nNumOfProcess, pDlg->m_Socket.message[59].fCPUUsage
			, (int)pDlg->m_Socket.message[59].dMemTotal, (int)pDlg->m_Socket.message[59].dAvailMemTotal, (int)pDlg->m_Socket.message[59].dVirtualTotal);
		
		pDlg->m_Socket.Send((LPVOID)(LPCTSTR)sendMessage, sendMessage.GetLength() * 2);
		
		listMesage.Format(_T("운영체제: %s, 프로세서 수: %d, cpu 사용률: %.1f, 메모리 전체 크기(MB): %d, 사용 가능 메모리(MB): %d, 가상 메모리(MB): %d")
			, pDlg->m_strWindowsVersion, pDlg->m_nNumOfProcess, pDlg->m_fCPUUsage, (int)pDlg->m_dMemTotal, (int)pDlg->m_dAvailMemTotal, (int)pDlg->m_dVirtualTotal);
		pDlg->m_List.AddString(listMesage);
		pDlg->m_List.SetCurSel(pDlg->m_List.GetCount() - 1);

		Sleep(1000);
	}

	return 0;
}

void CSystemInfoTransfererClientDlg::DrawCPUGraph() {
	CDC *pDC = GetDC();
	CPen *oldPen;
	CPen newPen;
	int graphStartX = (m_DlgSize.right - m_DlgSize.left - (GRAPH_WIDTH + GRAPH_WIDTH / 60)) / 2;
	int graphStartY = CPUGRAPHTOPY;
	int graphEndX = graphStartX + GRAPH_WIDTH;
	int graphEndY = graphStartY + GRAPH_HEIGHT;

	pDC->SetBkMode(TRANSPARENT); // 글씨 배경 투명
	pDC->TextOutW(graphStartX, graphStartY - 30, _T("CPU 사용률(%)"));

	// 흰색 사각형 바탕 칠하기
	pDC->Rectangle(graphStartX, graphStartY, graphEndX + GRAPH_WIDTH / 60, graphEndY);

	// 파란색 사각형 틀 그리기
	newPen.CreatePen(PS_SOLID, 2, RGB(38, 168, 217));
	oldPen = pDC->SelectObject(&newPen);

	pDC->MoveTo(graphStartX, graphStartY);
	pDC->LineTo(graphStartX, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY);
	pDC->LineTo(graphStartX, graphStartY);

	pDC->TextOutW(graphStartX, graphEndY + 10, _T("60초"));
	pDC->TextOutW(graphEndX, graphEndY + 10, _T("1초"));

	// 눈금선 그리기
	CPen linePen;
	linePen.CreatePen(PS_SOLID, 1, RGB(255, 128, 192));
	pDC->SelectObject(linePen);

	for (int i = 1; i <= 9; i++) {
		pDC->MoveTo(graphStartX, graphStartY + GRAPH_HEIGHT / 10 * i);
		pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY + GRAPH_HEIGHT / 10 * i);
	}

	linePen.DeleteObject();

	// 그래프 그리기
	for (int i = 0; i < 60; i++) {
		if (m_Socket.message[i].dMemInUse > 0) {
			int x = (GRAPH_WIDTH / 60) * (i + 1) + graphStartX;
			double lineLength = m_Socket.message[i].fCPUUsage*GRAPH_HEIGHT / 100;
			int y = graphEndY - lineLength;

			pDC->SelectObject(&newPen);
			pDC->MoveTo(x, graphEndY);
			pDC->LineTo(x, y);
		}
	}

	pDC->SelectObject(oldPen);

	newPen.DeleteObject();

	ReleaseDC(pDC);
}

void CSystemInfoTransfererClientDlg::DrawMemoryGraph() {
	CDC *pDC = GetDC();
	CPen *oldPen;
	CPen newPen;
	int graphStartX = (m_DlgSize.right - m_DlgSize.left - (GRAPH_WIDTH + GRAPH_WIDTH / 60)) / 2;
	int graphStartY = MEMORYGRAPHTOPY;
	int graphEndX = graphStartX + GRAPH_WIDTH;
	int graphEndY = graphStartY + GRAPH_HEIGHT;

	pDC->SetBkMode(TRANSPARENT); // 글씨 배경 투명
	pDC->TextOutW(graphStartX, graphStartY - 30, _T("메모리 사용률(%)"));

	// 흰색 사각형 바탕 칠하기
	pDC->Rectangle(graphStartX, graphStartY, graphEndX + GRAPH_WIDTH / 60, graphEndY);

	// 빨간색 사각형 틀 그리기
	newPen.CreatePen(PS_SOLID, 2, RGB(220, 10, 104));
	oldPen = pDC->SelectObject(&newPen);

	pDC->MoveTo(graphStartX, graphStartY);
	pDC->LineTo(graphStartX, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY);
	pDC->LineTo(graphStartX, graphStartY);

	pDC->TextOutW(graphStartX, graphEndY + 10, _T("60초"));
	pDC->TextOutW(graphEndX, graphEndY + 10, _T("1초"));

	// 눈금선 그리기
	CPen linePen;
	linePen.CreatePen(PS_SOLID, 1, RGB(116, 222, 220));
	pDC->SelectObject(linePen);

	for (int i = 1; i <= 9; i++) {
		pDC->MoveTo(graphStartX, graphStartY + GRAPH_HEIGHT / 10 * i);
		pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY + GRAPH_HEIGHT / 10 * i);
	}

	linePen.DeleteObject();

	// 그래프 그리기
	for (int i = 0; i < 60; i++) {
		if (m_Socket.message[i].dMemInUse > 0) {
			int x = (GRAPH_WIDTH / 60) * (i + 1) + graphStartX;
			double lineLength = (m_Socket.message[i].dMemInUse / m_Socket.message[i].dMemTotal)*GRAPH_HEIGHT;
			int y = graphEndY - lineLength;

			pDC->SelectObject(&newPen);
			pDC->MoveTo(x, graphEndY);
			pDC->LineTo(x, y);
		}
	}

	newPen.DeleteObject();

	ReleaseDC(pDC);
}

void CSystemInfoTransfererClientDlg::SetMemoryInfo() {
	MEMORYSTATUSEX memoryStatus;

	memset(&memoryStatus, sizeof(MEMORYSTATUSEX), 0);

	memoryStatus.dwLength = sizeof(MEMORYSTATUSEX);

	GlobalMemoryStatusEx(&memoryStatus);

	m_dMemTotal = (memoryStatus.ullTotalPhys / 1024) / 1024;
	m_dAvailMemTotal = (memoryStatus.ullAvailPhys / 1024) / 1024;
	m_dVirtualTotal = (memoryStatus.ullTotalPageFile / 1024) / 1024;
}

void CSystemInfoTransfererClientDlg::SetProcessorInfo() {
	SYSTEM_INFO sysInfo;

	GetSystemInfo(&sysInfo);

	m_fCPUUsage = cpu.usage_f();
	m_nNumOfProcess = sysInfo.dwNumberOfProcessors;
}

void CSystemInfoTransfererClientDlg::SetWindowsVersionInfo()
{
	OSVERSIONINFOEX osVersionInfoEx;

	ZeroMemory(&osVersionInfoEx, sizeof(OSVERSIONINFOEX));
	osVersionInfoEx.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

	GetVersionEx((LPOSVERSIONINFOW)&osVersionInfoEx);

	switch (osVersionInfoEx.dwMajorVersion) {
	case 5:
		switch (osVersionInfoEx.dwMinorVersion) {
		case 1:
			m_strWindowsVersion.Format(_T("Windows XP"));

			break;
		case 2:
			if (osVersionInfoEx.wProductType == VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows XP Professional x64 Edition"));

				break;
			}
			else if (GetSystemMetrics(SM_SERVERR2) == 0)
			{
				m_strWindowsVersion.Format(_T("Windows Server 2003"));

				break;
			}
			else if (osVersionInfoEx.wSuiteMask & VER_SUITE_WH_SERVER)

			{
				m_strWindowsVersion.Format(_T("Windows Home Server"));

				break;
			}
			else if (GetSystemMetrics(SM_SERVERR2) != 0)
			{
				m_strWindowsVersion.Format(_T("Windows Server 2003 R2"));

				break;
			}

			break;
		default:
			m_strWindowsVersion.Format(_T("Unknow OS"));
		}

		break;

	case 6:
		switch (osVersionInfoEx.dwMinorVersion) {
		case 0:
			if (osVersionInfoEx.wProductType == VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows Vista"));

				break;
			}
			else if (osVersionInfoEx.wProductType != VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows Server 2008"));

				break;
			}

			break;
		case 1:
			if (osVersionInfoEx.wProductType != VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows Server 2008 R2"));

				break;
			}
			else if (osVersionInfoEx.wProductType == VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows 7"));

				break;
			}

			break;
		case 2:
			if (osVersionInfoEx.wProductType != VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows Server 2012"));

				break;
			}
			else if (osVersionInfoEx.wProductType == VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows 8 and Over"));

				break;
			}

			break;
		case 3:
			if (osVersionInfoEx.wProductType != VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows Server 2012 R2"));

				break;
			}
			else if (osVersionInfoEx.wProductType == VER_NT_WORKSTATION)
			{
				m_strWindowsVersion.Format(_T("Windows 8.1"));

				break;
			}

			break;
		default:
			m_strWindowsVersion.Format(_T("Unknown OS"));
		}

		break;

	case 10:
		if (osVersionInfoEx.wProductType == VER_NT_WORKSTATION)
		{
			m_strWindowsVersion.Format(_T("Windows 10"));
		}

		break;

	default:
		m_strWindowsVersion.Format(_T("Unknown OS"));

		break;
	}
}
