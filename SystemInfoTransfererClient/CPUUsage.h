#ifndef __CPUU_H__
#define __CPUU_H__

#ifndef _WIN32
#error "This source code only runs under Windows platfrom."
#endif

#include <windows.h>

class CCPUUsage
{
public:
	CCPUUsage();

public:
	short  usage() { ReadCpu(); return _nCPUv; }
	float  usage_f() { ReadCpu(); return _fCPUv; }

private:
	ULONGLONG SubtractTimes(const FILETIME& ftA, const FILETIME& ftB);
	bool      enoughTimePassed();

protected:
	void ReadCpu();

private:
	inline
		bool firstrun() const { return (_dwLastRun == 0); }

private:
	FILETIME    _ftPrevSysKernel;
	FILETIME    _ftPrevSysUser;
	FILETIME    _ftPrevSysIdle;

	short       _nCPUv;
	float       _fCPUv;
	DWORD       _dwLastRun;

private:
	volatile
		LONG        _lRunCount;
};

#endif /// of __CPUU_H__