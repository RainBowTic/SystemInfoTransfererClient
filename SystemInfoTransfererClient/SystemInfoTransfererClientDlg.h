/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// SystemInfoTransfererClientDlg.h : 헤더 파일
//

#pragma once
#pragma warning(disable:4996)

#include "ConnectSocket.h"
#include "afxwin.h"
#include "CPUUsage.h"
#include "rapidjson\document.h"
#include "rapidjson\writer.h"
#include "rapidjson\stringbuffer.h"

#define LOCALHOST "127.0.0.1"
#define SERVERIP "211.208.41.135"
#define PORTNUM 21000
#define GRAPH_WIDTH 420.0
#define GRAPH_HEIGHT 120.0
#define CPUGRAPHTOPY 380
#define MEMORYGRAPHTOPY 570

using namespace rapidjson;

// CSystemInfoTransfererClientDlg 대화 상자
class CSystemInfoTransfererClientDlg : public CDialogEx
{
// 생성입니다.
public:
	CSystemInfoTransfererClientDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SYSTEMINFOTRANSFERERCLIENT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedConnect();
	afx_msg void OnBnClickedSend();
	afx_msg void OnBnClickedDisconnect();
	afx_msg void OnBnClickedClose();
	DECLARE_MESSAGE_MAP()
	void SetMemoryInfo();
	void SetProcessorInfo();
	void SetWindowsVersionInfo();
	void DrawMemoryGraph();
	void DrawCPUGraph();
	static UINT SendPCInfo(LPVOID pParam);

public:
	CConnectSocket m_Socket;
	CListBox m_List;
	UINT m_nNumOfProcess;
	CString m_strWindowsVersion;
	CCPUUsage cpu;
	CWinThread* pThread;
	CButton m_btnSend;
	CRect m_DlgSize;

	double m_dMemTotal;
	double m_dAvailMemTotal;
	double m_dVirtualTotal;
	float m_fCPUUsage;
};