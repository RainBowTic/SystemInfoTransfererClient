/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// ConnectSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SystemInfoTransfererClient.h"
#include "ConnectSocket.h"
#include "SystemInfoTransfererClientDlg.h"

// CConnectSocket

CConnectSocket::CConnectSocket()
{
}

CConnectSocket::~CConnectSocket()
{
}

// CConnectSocket 멤버 함수

void CConnectSocket::OnClose(int nErrorCode)
{
	CSystemInfoTransfererClientDlg *pMain = (CSystemInfoTransfererClientDlg*)AfxGetMainWnd();

	m_bIsConnected = false;
	pMain->m_List.AddString(_T("서버와의 연결이 끊어졌습니다."));
	pMain->m_List.SetCurSel(pMain->m_List.GetCount() - 1);

	if (m_bIsSending) {
		m_bIsSending = false;
		pMain->m_btnSend.SetWindowTextW(_T("보내기"));
	}

	ShutDown();
	Close();

	CSocket::OnClose(nErrorCode);

	AfxMessageBox(_T("ERROR: Disconnected from server!"));
	//::PostQuitMessage(0);
}