#pragma once

#include "stdafx.h"

typedef struct SendMessage {
	CString strWindowsVersion = nullptr;
	UINT nNumOfProcess = 0;
	float fCPUUsage = -1;
	double dMemTotal = -1;
	double dAvailMemTotal = -1;
	double dVirtualTotal = -1;
	double dMemInUse = -1;
} SM;