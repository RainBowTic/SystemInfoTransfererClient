/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

#pragma once

#include "SendMessage.h"

// CConnectSocket 명령 대상입니다.

class CConnectSocket : public CSocket
{
public:
	CConnectSocket();
	virtual ~CConnectSocket();
	virtual void OnClose(int nErrorCode);
public:
	bool m_bIsConnected;
	bool m_bIsSending;
	SM message[60];
};